# Audio

## Ton und Schall

Ein Ton, aber auch ein Geräusch oder ein Klang, wird als Schwankung des Luftdrucks übertragen – darstellbar als Schallwelle:

![©](./sound-waves.png)

Die Stimmgabel schwingt und erzeugt eine Schallwelle. Diese wird im Medium Luft übertragen und erreicht so unser Ohr. Dort bringt sie das Trommelfell zum Schwingen – wir hören den Ton.

Je nach Art des Klangs ergeben sich verschiedene Kurven:

![©](./sound-types.png)

## Wie abspeichern?

Wie können wir nun so einen Klang abspeichern? Zuerst müssen wir natürlich ein künstliches Trommelfell haben – also ein Mikrofon. Dieses übersetzt die Druckschwankungen der Luft in ein elektrisches Signal.

### Analog - Schallplatte

Auf einer Schallplatte wird das Signal analog abgespeichert. Die Rillen entsprechen den Schallwellen. Die Nadel tastet diese ab und erzeugt daraus wieder einen Ton.

::: columns 2
![Schallplatte ©](./turntable.jpg)

---

![Schallplatte unter dem Rasterelektronenmikroskop<br>Ausschnitt: 200 μm ©](./vinyl-record-microscope.jpg)
:::

### Digital

Wie kann eine Schallwelle als Abfolge von 0 und 1 gespeichert werden? Dazu wird die Amplitude der Schallwelle, welche vom Mikrofon in ein elektrisches Singal übersetzt wurde, in regelmässigen Abständen gemessen und der aktuelle Wert als Zahl gespeichert.

::: columns 2
![CD-Spieler ©](./cd-player.jpg)

---

![CD unter dem Rasterelektronenmikroskop<br>Ausschnitt: 15 μm ©](./cd-microscope.jpg)
:::

Dabei beeinflussen zwei Werte die Qualität der Aufnahme:

- Die **Abtastrate** gibt an, wie oft pro Sekunde die Amplitude gemsessen wird. Die Abtastrate wird in **Hertz** angegeben ein typischer Wert ist **44.1 kHz**.
- Die **Bittiefe** gibt an, wieviele Bits für die Darstellung eines Werts zu Verfügung stehen. Sie bestimmt, in wie vielen Abstufungen die Amplitude repräsentiert werden kann. Sie entspricht der Farbtiefe bei Pixelgrafiken. Eine typische Bittiefe ist **16 Bit**.

::: columns 2
![unterschiedliche Abtastraten](./sample-rate.png)

---

![unterschiedliche Bittiefen](./bit-depth.png)
:::

<!--
::: exercise
#### :extra: Audacity
Installieren Sie Audacity auf Ihrem Laptop:

* [:download: Audacity 2.4.2 für Windows](https://www.fosshub.com/Audacity.html/audacity-win-2.4.2.exe)
* [:download: Audacity 2.4.2 für macOS](https://www.fosshub.com/Audacity.html/audacity-macos-2.4.2.dmg)

Testen Sie die grundlegenden Funktionen aus. Hier ist eine kurze Videoanleitung:

<v-video source="youtube" id="mAhL5pbl98s">
Kurzanleitung zu Audacity
</v-video>

Wo in Audacity können Sie die Abtastrate und Bittiefe sehen und ändern?
:::
-->
