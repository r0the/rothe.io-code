# Information und Daten

::: warning
09.02.2022 Das Kapitel «Grafikformate» ist komplett überarbeitet worden.
:::

## Inhalt

- [1. Grundlagen](?page=1-basics/)
- [2. Zahlensysteme](?page=2-number/)
- [3. Textcodierung](?page=3-text/)
- [4. Grafikformate](?page=4-graphics/)
- [5. Kompression](?page=5-compression/)
- [6. Audio und Video](?page=6-audio-video/)

## Zusammenfassung

Für die Verarbeitung im Computer werden Informationen **digitalisiert**, d.h. sie werden in klar unterscheidbare Zustände eingeteilt.

Bei der **binären** Darstellung werden nur zwei Grundzeichen verwendet. Im Gegensatz zur dezimalen Darstellung mit zehn Grundzeichen kann die binäre technisch viel einfacher umgesetzt werden.

Ein **Code** ist eine Vorschrift, wie Informationen in binäre Daten übersetzt werden.

Die kleinste binäre Einheit heisst **Bit**. Acht Bits bilden ein **Byte**. Die Anzahl Byte, welche benötigt werden, um eine Information als binäre Daten darzustellen, heisst **Datenmenge**.
