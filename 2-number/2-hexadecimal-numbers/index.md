# 2.2 Hexadezimalsystem

Das Hexadezimalsystem ist ein weiteres wichtiges Zahlensystem in der Informatik. Seine Basis ist 16. Das bedeutet, dass es im Hexadezimalsystem 16 Ziffern gibt:

$$0, 1, 2, 3, 4, 5, 6, 7, 8, 9, A, B, C, D, E, F$$

Dabei haben die Ziffern $A$ bis $F$ den dezimalen Wert $10$ bis $15$. Die hexadezimale Zahl $2F_{16}$ wird so in das Dezimalsystem umgerechnet:

$$2F_{16} = 2 \cdot 16^1 + 15 \cdot 16^0 = 32 + 15 = 47$$

Auch hier wird der hexadezimalen Zahl die tiefgestellte Basis $_{16}$ angehängt, um sie von einer Dezimalzahl unterscheiden zu können.

## Umrechnung zwischen Binär- und Hexadezimalsystem

Besonders einfach ist die Umrechnung zwischen dem Binär- und dem Hexadezimalsystem. Eine Ziffer des Hexadezimalsystems entspricht immer vier Ziffern des Binärsystems. Die folgende Tabelle zeigt die dezimalen und binären Werte der 16 hexadezimalen Ziffern:

| Hexadezimal | Dezimal |  Binär   |
| :---------: | :-----: | :------: |
|  $0_{16}$   |   $0$   | $0000_2$ |
|  $1_{16}$   |   $1$   | $0001_2$ |
|  $2_{16}$   |   $2$   | $0010_2$ |
|  $3_{16}$   |   $3$   | $0011_2$ |
|  $4_{16}$   |   $4$   | $0100_2$ |
|  $5_{16}$   |   $5$   | $0101_2$ |
|  $6_{16}$   |   $6$   | $0110_2$ |
|  $7_{16}$   |   $7$   | $0111_2$ |
|  $8_{16}$   |   $8$   | $1000_2$ |
|  $9_{16}$   |   $9$   | $1001_2$ |
|  $A_{16}$   |  $10$   | $1010_2$ |
|  $B_{16}$   |  $11$   | $1011_2$ |
|  $C_{16}$   |  $12$   | $1100_2$ |
|  $D_{16}$   |  $13$   | $1101_2$ |
|  $E_{16}$   |  $14$   | $1110_2$ |
|  $F_{16}$   |  $15$   | $1111_2$ |
