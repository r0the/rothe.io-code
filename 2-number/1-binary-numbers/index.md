# 2.1 Binärsystem[^1]

Im **Binärsystem** oder **Dualsystem** stellen wir Zahlen mit nur zwei Ziffern dar. Das Binärsystem ist somit ein Binärcode für Zahlen. An stelle der zehn Ziffern ($0$ bis $9$) des Dezimalsystems stehen uns nur zwei Ziffern ($0$ und $1$) zur Verfügung. Wenn wir also im Binärsystem zählen, dann benötigen wir bereits für die Zahl $2$ zwei Stellen:

|  Binär | Dezimal |   Binär | Dezimal |    Binär | Dezimal |    Binär | Dezimal |
| -----: | :------ | ------: | :------ | -------: | :------ | -------: | :------ |
|  $0_2$ | $0$     | $100_2$ | $4$     | $1000_2$ | $8$     | $1100_2$ | $12$    |
|  $1_2$ | $1$     | $101_2$ | $5$     | $1001_2$ | $9$     | $1101_2$ | $13$    |
| $10_2$ | $2$     | $110_2$ | $6$     | $1010_2$ | $10$    | $1110_2$ | $14$    |
| $11_2$ | $3$     | $111_2$ | $7$     | $1011_2$ | $11$    | $1111_2$ | $15$    |

Bei Binärzahlen hängen wir immer eine kleine, tiefgestellte $_2$ an, um sie von Dezimalzahlen unterscheiden zu können.

## Stellenwert

Im Dezimalsystem nimmt der Wert der Stelle von rechts nach links immer um den Faktor 10 zu. So ist $100$ zehn mal mehr als $10$.

**Beispiel:** Die Zahl 2037 besteht aus 2 Tausendern, keinem Hunderter, 3 Zehner und 7 Einern:

![](./stellenwert-dezimal.svg)

Wir können den Wert der Stelle auch als 10er-Potenz schreiben:

$$ \overset{10^3}{2} \quad \overset{10^2}{0} \quad \overset{10^1}{3} \quad \overset{10^0}{7}$$

Zehn ist die **Basis** des Dezimalsystems.

::: info
| Stelle | Tausender | Hunderter | Zehner | Einer |
|:----------- | -------------:| ------------:| -----------:| ----------:|
| Nummer | $3$ | $2$ | $1$ | $0$ |
| Stellenwert | $10^3$ | $10^2$ | $10^1$ | $10^0$ |
| Stellenwert | $1000$ | $100$ | $10$ | $1$ |
| «Ziffer» | $2$ | $0$ | $3$ | $7$ |
| Wert | $2\cdot 1000$ | $0\cdot 100$ | $3\cdot 10$ | $7\cdot 1$ |

- Der **Stellenwert** ist die Basis des Systems hoch die Nummer der Stelle beginnend mit Null.
- Der **Wert** einer Ziffer ist die Ziffer mal den Stellenwert.

:::

## Binärzahlen

Im Binärsystem ist dieser Faktor einfach 2. Wir können also die Werte der Stellen als Zweierpotenz schreiben. Für die binäre Zahl $1101_2$ sieht das also so aus:

$$ \overset{2^3}{1} \quad \overset{2^2}{1} \quad \overset{2^1}{0} \quad \overset{2^0}{1}$$

Oder mit ausgerechneten Stellenwerten:

$$ \overset{8}{1} \quad \overset{4}{1} \quad \overset{2}{0} \quad \overset{1}{1}$$

## Zweierpotenzen

Wie gross ist $2^{10}$? Man muss das nicht auswendig können – wir können ja die das nächste Element der Zweierpotenzreihe ganz einfach aufs vorherige zurückführen:

$$ 2^n = 2 \cdot 2^{n-1} $$

Die Binärdarstellung der Zweierpotenz $2^n$ ist eine Eins gefolgt von $n$ Nullen.

|   Potenz |   Wert |       Binärzahl |
| -------: | -----: | --------------: |
|    $2^0$ |    $1$ |           $1_2$ |
|    $2^1$ |    $2$ |          $10_2$ |
|    $2^2$ |    $4$ |         $100_2$ |
|    $2^3$ |    $8$ |        $1000_2$ |
|    $2^4$ |   $16$ |       $10000_2$ |
|    $2^5$ |   $32$ |      $100000_2$ |
|    $2^6$ |   $64$ |     $1000000_2$ |
|    $2^7$ |  $128$ |    $10000000_2$ |
|    $2^8$ |  $256$ |   $100000000_2$ |
|    $2^9$ |  $512$ |  $1000000000_2$ |
| $2^{10}$ | $1024$ | $10000000000_2$ |

## Umrechnung vom Binär- ins Dezimalsystem[^2]

Um eine Binärzahl in die entsprechende Dezimalzahl umzurechnen, werden alle Ziffern jeweils mit ihrem Stellenwert (entsprechende Zweierpotenz) multipliziert und dann addiert.

Beispiel:
$$1101_2 = 1\cdot 2^3 + 1\cdot 2^2 + 0\cdot 2^1 + 1\cdot 2^0 = 8 + 4 + 0 + 1 = 13$$

Endet die Binärzahl mit einer 1, so ist die Dezimalzahl eine ungerade Zahl. Ist die letzte Ziffer der Binärzahl eine 0, so ist die Dezimalzahl gerade.

Beispiel:

$$101001_2 = 1\cdot 2^5 + 0\cdot 2^4 + 1\cdot 2^3 + 0\cdot 2^2 + 0\cdot 2^1 + 1\cdot 2^0 = 32+8+1 = 41 $$

$$101000_2 = 1\cdot 2^5 + 0\cdot 2^4 + 1\cdot 2^3 + 0\cdot 2^2 + 0\cdot 2^1 + 0\cdot 2^0 = 32+8 = 40 $$

Dieses Verfahren kann auch in Form einer Tabelle aufgeschrieben werden. Dazu notiert man die einzelnen Ziffern einer Binärzahl in Spalten, die mit dem jeweiligen Stellenwert der Ziffer überschrieben sind.

| Binärzahl |  32 |  16 |   8 |   4 |   2 |   1 |       Dezimalzahl |
| --------: | --: | --: | --: | --: | --: | --: | ----------------: |
|       101 |     |     |     |   1 |   0 |   1 |       $4 + 1 = 5$ |
|    100011 |   1 |   0 |   0 |   0 |   1 |   1 | $32 + 2 + 1 = 35$ |
|      1010 |     |     |   1 |   0 |   1 |   0 |      $8 + 2 = 10$ |

Man addiert nun alle Stellenwerte, die über den Einsen der Binärzahl stehen und erhält die entsprechende grün hinterlegte Dezimalzahl. Um zum Beispiel den Dezimalwert der dritten Binärzahl zu errechnen, werden die Stellenwerte 8 und 2 addiert. Das Ergebnis ist 10.

## Umrechnung vom Dezimal- ins Binärsystem (Variante «nach Rezept»)

Um eine Dezimalzahl ins Binärsystem umzurechnen, wird die Zahl wiederholt ganzzahlig (d.h. mit Rest) durch Zwei dividiert. Dann betrachtet man insbesondere den Rest des Resultats:

- Bei Rest $1$ schreibt man eine 1 hin.
- Bei Rest $0$ schreibt man eine 0 hin.
  Das Resultat wird nun wieder durch Zwei dividiert. Die nächste 0 oder 1 wird **links** der bestehenden hingeschrieben.
  Hier wird die Umrechnung am Beispiel 41 gezeigt:

$$
\begin{aligned}
41 : 2 &= 20\ Rest\ 1 \quad &\rightarrow \quad \text{1}\\
20 : 2 &= 10\ Rest\ 0 \quad &\rightarrow \quad \text{01}\\
10 : 2 &=  5\ Rest\ 0 \quad &\rightarrow \quad \text{001}\\
 5 : 2 &=  2\ Rest\ 1 \quad &\rightarrow \quad \text{1001}\\
 2 : 2 &=  1\ Rest\ 0 \quad &\rightarrow \quad \text{01001}\\
 1 : 2 &=  0\ Rest\ 1 \quad &\rightarrow \quad \text{101001}\\
\end{aligned}
$$

Die Binärzahl ergibt sich, indem die Reste **von unten nach oben** aufgeschrieben werden: **101001**.

## Umrechnung vom Dezimal- ins Binärsystem (Variante «Subtraktion»)

Für kleinere Zahlen ist diese Variante wohl schneller. Man muss jedoch die Zweierpotenzreihe gut auswendig kennen.

1. Nächst kleinere Zweierpotenz finden.
2. Diese von der Zahl abziehen, eine 1 notieren.
3. Kann nächstkleinere Zweierpotenz abgezogen werden?

- ja: abziehen und 1 notieren
- nein: eine 0 notieren
  4 Schritt 3 wiederholen bis kleinste Zweierpotenz

**Beispiel**

|   Dezimalzahl |                         Überlegung | Binärzahl |
| ------------: | ---------------------------------: | --------: |
|          $19$ | nächstkleinere Zweierpotenz ist 16 |         1 |
| $19 - 16 = 3$ |                  8 kommt nicht vor |         0 |
|           $3$ |                  4 kommt nicht vor |         0 |
|           $3$ |                        2 kommt vor |         1 |
|   $3 - 2 = 1$ |                        1 kommt vor |         1 |

[^1]: Quelle: [Sebastian Forster](https://informatik.mygymer.ch/g23c/003.daten-und-information/003.binaer-dezimal.html#dezimalzahlen-in-binarzahlen-umrechnen)
[^2]: Quelle: [Wikipedia: Dualsystem](https://de.wikipedia.org/wiki/Dualsystem)
