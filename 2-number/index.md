# 2 Zahlensysteme

## Inhalt

- [2.1 Binärsystem](?page=1-binary-numbers/)
- [2.2 Hexadezimalsystem](?page=2-hexadecimal-numbers/)
- [:extra: 2.3 Binäre Rationale Zahlen](?page=3-float/)
