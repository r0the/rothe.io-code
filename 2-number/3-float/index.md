# :extra: 2.3 Binäre Rationale Zahlen

Beim Rechnen mit rationalen Zahlen im Taschenrechner oder Computer gibt es häufig «Rundungsfehler». Wenn man beispielsweise im Computer $0.1 + 0.2$ rechnet, erhält man nicht genau $0.3$:

$$0.1 + 0.2 \neq 0.3$$

Die Grund dafür ist die binäre Darstellung der rationalen Zahlen.

## Dezimalsystem

Wir wissen inzwischen, dass unser Zahlensystem ein Stellenwertsystem ist:

$$125 = 1 \cdot 10^2 + 2 \cdot 10^1 + 5 \cdot 10^0$$

Das gilt genau so für rationale Zahlen:

$$1.25 = 1 \cdot 10^0 + 2 \cdot 10^{-1} + 5 \cdot 10^{-2}$$

## Binärsystem

Auch im Binärsystem können wir natürliche Zahlen:

$$5 = 1 \cdot 2^2 + 0 \cdot 2^1 + 1 \cdot 2^0 = 101_2$$

und eben auch rationale Zahlen darstellen:

$$1.25 = \frac{1}{1} + \frac{0}{2} + \frac{1}{4} = 1 \cdot 2^0 + 0 \cdot 2^{-1} + 1 \cdot 2^{-2} = 1.01_2$$

## Darstellung von 0.1

Nun stellt sich die Frage, wie 0.1 als binäre Zahl dargestellt werden kann. Das Problem ist, dass das nicht mit endlich vielen Ziffern möglich ist, so wie ein Drittel nicht als dezimale rationale Zahl mit einer endlichen Anzahl Ziffern dargestellt werden kann:

$$0.1 = 0.00011001100110011001100110011\ldots_2 $$

Da im Computer nur endlich viele Ziffern dargestellt werden können, muss die binäre Darstellung von 0.1 irgendwo abgeschnitten werden. Wird der abgeschnittene Wert wieder ins Dezimalsystem umgerechnet, erhält man nicht genau 0.1, sondern z.B.

$$0.100000000000000005551115$$
