# Informationsgehalt

## Buchstabenhäufigkeit

Die Buchstabenhäufigkeit gibt an, wie oft ein Buchstabe in einem Text vorkommen. Die Häufigkeit der einzelnen Buchstaben ist je nach Sprache unterschiedlich. Das folgende Diagramm zeigt die durchschnittliche Buchstabenhäufigkeit in deutschen Texten[^1]:

![](./letter-frequency-de.svg)

## Informationsgehalt

Wir führen ein Experiment durch. Aus einem deutschen Text werden etwa 40% der Buchstaben entfernt. Zuerst werden die vier häufigsten Buchstaben E, N, R und I entfernt. Danach werden diese Buchstaben sowie T, S und A beibehalten und die restlichen entfernt. Obschon etwa die gleiche Menge Buchstaben entfernt worden sind, bleibt der Text nach dem Entfernen der häufigen Buchstaben verständlicher.

<v-redundancy/>

Offenbar tragen die häufigen Buchstaben weniger zum Inhalt des Textes bei. Sie haben einen kleineren **Informationsgehalt**.

<!--
::: exercise Aufgabe

In HAFUM-Land gibt es nur fünf Buchstaben: H, A, F, U und M. Die Buchstaben kommen in der HAFUM-Sprache unterschiedlich häufig vor:

|  H  |  A  |  F  |  U  |  M  |
|:---:|:---:|:---:|:---:|:---:|
| 14% | 25% | 18% | 25% | 18% |

In HAFUM-Land ist die Internetanbindung sehr langsam, deshalb wollen die HAFUManer eine neue HUFMA-Codierung entwickeln. Es werden folgende Vorschläge eingereicht:

A) E = 000, S = 01, R = 1, A = 0010, M = 0011


A) E = *** S = *- R = - O = -* M = *
B) E = - S = * R = *** O = -* M = ---
C) E = ** S = *- R = * O = -* M = -
D) E = *- S = * R = - O = -* M = *

Welche Codierung ergibt die kleinsten Datenmengen?
:::
-->

::: exercise

#### :exercise: Zusatzaufgabe

1. Erstelle den Huffman-Code für die durchschnittliche Buchstabenhäufigkeit in deutschen Texten gemäss folgender Tabelle:

|     |        |     |       |     |       |     |       |     |        |
| --: | :----- | --: | :---- | --: | :---- | --: | :---- | --- | ------ |
|   E | 15.99% |   A | 6.34% |   O | 2.75% |   W | 1.40% | J   | 0.27 % |
|   N | 9.59%  |   D | 4.92% |   M | 2.75% |   Z | 1.22% | Ö   | 0.24 % |
|   R | 7.71%  |   H | 4.11% |   C | 2.71% |   P | 1.06% | ß   | 0.15 % |
|   I | 7.60%  |   U | 3.76% |   B | 2.21% |   V | 0.94% | Y   | 0.13 % |
|   T | 6.43%  |   L | 3.72% |   F | 1.80% |   Ü | 0.63% | X   | 0.07 % |
|   S | 6.41%  |   G | 3.02% |   K | 1.50% |   Ä | 0.54% | Q   | 0.04 % |

2. Codiere einen kurzen Text mit diesem Code.

---

![](./huffman-german.svg)

| Zeichen | Code      | Zeichen | Code        |
| ------: | :-------- | ------: | :---------- |
|  (leer) | 100       |       O | 01001       |
|       A | 0101      |       P | 1010111     |
|       B | 111000    |       Q | 11100110111 |
|       C | 01111     |       R | 1111        |
|       D | 0110      |       S | 0011        |
|       E | 110       |       T | 0010        |
|       F | 000110    |       U | 11101       |
|       G | 01000     |       V | 1110010     |
|       H | 10100     |       W | 1010100     |
|       I | 0000      |       X | 11100110110 |
|       J | 111001100 |       Y | 1110011010  |
|       K | 000111    |       Z | 1010101     |
|       L | 000100    |       Ä | 10101101    |
|       M | 01110     |       Ö | 111001110   |
|       N | 1011      |       Ü | 10101100    |

:::

[^1]: [Practical Cryptography: German Letter Frequencies][1]

[1]: http://practicalcryptography.com/cryptanalysis/letter-frequencies-various-languages/german-letter-frequencies/
