# :exercise: Arbeitsblatt Huffman

- [:pdf: Arbeitsblatt als PDF](./arbeitsblatt-huffman.pdf)

::: exercise

#### :exercise: Decodieren

![](./huffman-anna.svg)

1. Decodiere diese Bitfolge mit dem obenstehenden Codebaum. Das Symbol ⎵ steht für das Leerzeichen.

   `0111101011000110110101`

---

`ANNAS ANANAS`
:::

::: exercise

#### :exercise: Huffman-Codierung 1

1. Erstelle zum Wort «MISSISSIPPI» eine Häufigkeitstabelle.
2. Erstelle einen Huffman-Baum
3. Codiere das Wort.

---

| Zeichen        | M   | P   | I   | S   |
| :------------- | :-- | :-- | :-- | :-- |
| **Häufigkeit** | 1   | 2   | 4   | 4   |

![](./huffman-mississippi-5.svg)

Codierung: `100110011001110110111`, Total 21 Bit
:::

::: exercise

#### :exercise: Huffman-Codierung 2

1. Erstelle zum Wort «EXTERNER EFFEKT» eine Häufigkeitstabelle.
2. Erstelle einen Huffman-Baum
3. Codiere das Wort.

---

| Zeichen        | X   | N   | K   | T   | R   | F   | E   |
| :------------- | :-- | :-- | :-- | :-- | --- | --- | --- |
| **Häufigkeit** | 1   | 1   | 1   | 2   | 2   | 2   | 5   |

![](./huffman-externereffekt.png)

Codierung: ``, Total Bit
:::
