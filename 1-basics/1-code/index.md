# 1.1 Begriffe

### Definitionen

::: info

#### Information

Unter einer **Information** versteht man «das Wissen, das ein Sender einem Empfänger über einen Informationskanal übermittelt».[^1]
:::

::: info

#### Daten

Daten sind Zeichen oder Symbole, die Informationen **darstellen**.
Daten können gesendet, empfangen und verarbeitet werden.
:::

::: info

#### Code

Ein Code ist eine «Sprache», die festlegt, wie Informationen vom Sender in Daten übersetzt und vom Empfänger wieder interpretiert werden.
:::

![](./code.svg)

### Code

Die Informationsübertragung kann nur funktionieren, wenn Sender und Empfänger den gleichen Code verwenden. Bei zwischenmenschlicher Kommunikation ist der Code normalerweise nicht explizit festgelegt. Dann muss der Empfänger den Code erraten, was zu Missverständnissen führen kann.

![](./cover.svg)

Ein Code ist eine Vorschrift, welche Daten von einer Darstellung in eine andere umwandelt. Dabei darf keine Information verloren gehen, **eine Rückumwandlung muss möglich** sein.

Beispiel eines Codes: Umwandlung von Braillezeichen in Buchstaben und umgekehrt.

![](./code-1.svg)

Das Ziel dieser Umwandlung ist es, eine für einen **bestimmten Zweck optimierte** Darstellung Informationen zu erreichen. So ist die Brailleschrift dafür optimiert, per Tastsinn gelesen werden zu können.

## Beispiele für Codes

::: cards 3
![](./braille.jpg)

#### Brailleschrift

---

![](./ascii.png)

#### ASCII

---

![](./dna-transcription.svg)

#### Genetischer Code

---

![](./qr-generator.png)

#### QR-Code

---

![](./isbn.svg)

#### ISBN

---

[![](./what3words.png)][1]

#### [what3words][1]

:::

## Binärer Code

Ein binärer Code wird verwendet, um Daten in eine **binäre Darstellung** umzuwandeln. Zweck einer binären Codierung ist es, die Daten mit einem Computer verarbeiten zu können.

[1]: https://what3words.com/kissen.stolz.ganzes

[^1]: Quelle: [Wikipedia: Information](https://de.wikipedia.org/wiki/Information)
