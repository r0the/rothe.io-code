# 1.3 Datenmenge

Der Raum, welche eine bestimmte Menge an Materie einnimmt, wird als deren Volumen bezeichnet und in Liter oder Kubikmeter gemessen. Analog ist die **Datenmenge** ist ein Mass für die **Anzahl Bits**, welche verwendet werden, um bestimmte Daten digital darzustellen.

Dabei kann die gleiche Information je nach Codierung eine unterschiedliche Datenmenge beanspruchen.

## Bit

Die Datenmenge, welche mit zwei Zuständen dargestellt werden kann, nennt man ein **Bit**.

## SI-Präfixe

Für Datenmengen werden die SI-Präfixe des Internationalen Einheitensystems (SI) verwendet, wie sie auch bei anderen Masseinheiten gebräuchlich sind (z.B. ein Kilogramm Brot, eine Megawattstunde, 34 Gigatonnen CO<sub>2</sub>-Emission pro Jahr).

| Bezeichnung | typisches Beispiel                    |  Kürzel |    Potenz |    Grösse |
| :---------- | :------------------------------------ | ------: | --------: | --------: |
| Bit         |                                       | $1$ bit |           |           |
| Byte        | ein Buchstabe                         |   $1$ B |    $10^0$ |   $8$ bit |
| Kilobyte    | eine halbe Seite Text                 |  $1$ kB |    $10^3$ |  $1000$ B |
| Megabyte    | eine Minute Musik (MP3)               |  $1$ MB |    $10^6$ | $1000$ kB |
| Gigabyte    | eine Viertelstunde Video (Full HD)    |  $1$ GB |    $10^9$ | $1000$ MB |
| Terabyte    | Speicherplatz einer Festplatte (2016) |  $1$ TB | $10^{12}$ | $1000$ GB |
| Petabyte    |                                       |  $1$ PB | $10^{15}$ | $1000$ TB |
| Exabyte     |                                       |  $1$ EB | $10^{18}$ | $1000$ PB |

## Historische Binärpräfixe

Weil Datenspeicher eine binäre _Adressierung_ verwenden, sind Speicherkapazitäten normalerweise Zweierpotenzen. Deshalb war es früher üblich, $2^{10} = 1024$ Byte als Kilobyte oder kB zu bezeichnen.

1996 hat die International Electrotechnical Commission (IEC) spezielle Präfixe für Binärpräfixe festgelegt:

| Bezeichnung |  Kürzel |   Potenz |     Grösse |
| :---------- | ------: | -------: | ---------: |
| Kibibyte    | $1$ kiB | $2^{10}$ | $1.024$ kB |
| Mebibyte    | $1$ MiB | $2^{20}$ |  $1.05$ MB |
| Gibibyte    | $1$ GiB | $2^{30}$ |  $1.07$ GB |
| Tebibyte    | $1$ TiB | $2^{40}$ |  $1.10$ TB |
| Pebibyte    | $1$ PiB | $2^{50}$ |  $1.12$ PB |

Auch heute kann man sich nicht immer sicher sein, ob die Angabe «1 TB» ein Terabyte, also $10^{12}$ Byte oder ein Tebibyte, also $2^{40}$ Byte bedeutet.
