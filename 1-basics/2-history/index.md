# 1.2 Geschichte

## Informationen weitergeben

Seit jeher haben Menschen Informationen weitergegeben. Dazu mussten die Informationen allerdings erst in eine Form gebracht werden, die zur Übertragung geeignet ist. Diese Darstellungsformen von Informationen haben sind im Laufe der Zeit massiv verändert:

|           Zeit | Informationsaustausch                                   |
| -------------: | :------------------------------------------------------ |
| 10'000 v. Chr. | Höhlenmalereien                                         |
|  5'000 v. Chr. | Knotenschrift (Quipu / Khipu)                           |
|  3'000 v. Chr. | Keilschrift, Hieroglyphen                               |
|  1'184 v. Chr. | Rauchtelegraf                                           |
|           1453 | Gutenberg druckt das erste Buch                         |
|           1610 | Erste Postkutsche für den Posttransport                 |
|           1837 | Morse meldet Patent für Telegrafen an                   |
|           1876 | Bell erfindet das Telefon                               |
|           1906 | Erstes Radioprogramm mit Sprach- und Musikübertragung   |
|           1929 | Erste Bauanleitung für einen Fernsehempfänger           |
|           1938 | Kornad Zuse baut den ersten Computer mit Relais         |
|           1946 | Erster vollelektronischer Rechner (Eniac)               |
|           1969 | ARPANET (Vorläufer des Internet)                        |
|           1971 | Intel verkauft den ersten Mikroprozessor                |
|           1979 | Erster Personal Computer auf dem Markt                  |
|           1989 | Erste mobile Handtelefone                               |
|           1989 | World Wide Web wird am CERN entwickelt                  |
|           1993 | Grafische Webbrowser machen das Internet massentauglich |

Um Informationen auszutauschen, werden Daten verwendet. Beispielsweise wurden in der Steinzeit Symbole an die Höhlenwände gemalt, später Buchstaben erfunden, um Informationen in geschriebener Form weiterzugeben und noch später diese Buchstaben in digitaler Form dargestellt, um sie über das Internet zu übertragen.

Aber nicht nur Menschen geben Informationen weiter. Nachfolgend ein Beispiel aus dem Reich der Bienen. Die Abbildung verdeutlicht, wie Bienen die Information – wo Nektar gefunden werden kann – an andere Bienen weitergeben.

![Bienentanz](./bee-dance.svg)

## Die Vorläufer der heutigen Computer (programmierbare Maschinen)

### Joseph-Marie Jacquard

Joseph-Marie Jacquard (1752 bis 1834) war ein französischer Erfinder, der durch seine Weiterentwicklung des programmierbaren Webstuhls entscheidend zur industriellen Revolution beitrug.

::: columns
![Joseph-Marie Jacquard ©](./joseph-marie-jacquard.jpg)

---

![Programmierung eines Jacquard-Webstuhls ©](./jacquard-loom-programming.jpg)

---

![Lochkarten in einem Jacquard-Webstuhl ©](./jacquard-loom-punchcards.jpg)
:::

Um auf einem Webstuhl einen Stoff mit einem Muster herzustellen, müssen bei einem Webstuhl bei jedem Durchgang des Schiffchens bestimmte Gruppen von Fäden angehoben werden. Jacquard entwickelte eine automatische, mechanische Steuerung für diesen Vorgang. Bei einem Jacquard-Webstuhl wird das Muster über Löcher in einer beliebig langen Kette von Lochkarten codiert. Dabei bedeutet ein Loch, dass der Faden gehoben wird, kein Loch, dass der Faden gesenkt wird. Die Lochkarten werden von Nadeln abgetastet und in die gewünschte Bewegung umgesetzt.[^1]

Dabei hat Jacquard die **[:fundamental: binäre Darstellung][1]** von Informationen vorweggenommen: Die Muster werden als Binärcode (Loch bzw. kein Loch) gespeichert. Die auf zwei Grundzuständen beruhende Darstellung von Informationen mit Materie oder Energie hat sich bis heute bewährt und durchgesetzt.

### Lochkarten und -streifen

Lochkarten und -streifen wurden und werden für verschiedene Zwecke verwendet. Typische Beispiele sind:

- Steuerung von Musikinstrumenten, z.B. Drehorgel
- Arbeitszeiterfassung mittels Stempeluhr
- Programmierung von Computern

::: columns 2
![Lochstreifen ©](./punched-tape.png)

---

![Standard-Lochkarte mit Programmcode ©](./punch-card.png)
:::

::: exercise

#### :exercise: Aufgabe

1. Was passiert, wenn Lochkarten mit einem Computerprogramm auf einem automatischen Musikinstrument oder einem Webstuhl abgespielt wird?
2. Wie kann man erkennen, ob es sich bei einem Lochstreifen um ein Computerprogramm, ein Webmuster oder ein Musikstück handelt?

:::

## Moderne Technologien

Binärcodes sind nicht an ein bestimmtes Medium gebunden, sondern überall dort realisierbar, wo der Wechsel zwischen zwei Zuständen erzeugt und festgestellt werden kann. Dadurch sind Binärcodes universell einsetzbar.[^2]

Heute werden Bits normalerweise nicht mehr durch Löcher in Papier dargestellt, sondern mit folgenden physikalischen Effekten:

::: columns 3
![elektromagnetische Wellen ©](./wave.jpg)

---

![elektrische Ladung ©](./batteries.jpg)

---

![elektrische Spannung ©](./lightning.jpg)

---

![Reflexion von Licht ©](./laser.jpg)

---

![Magnetisierung ©](./magnet.jpg)

---

![Quantenzustände ©](./atom.png)
:::

::: exercise

#### :exercise: Aufgabe

Kannst du erraten, welche der obenstehenden physikalischen Effekte die folgenden Technologien verwenden?

- DVD
- Glasfaserkabel
- Mikrochip
- Festplatte
- WLAN
- Quantencomputer
- Memory-Stick
- Bluetooth
- Leiterbahn
- SSD
- USB-Kabel
- Tonband
- CD
- Mobilfunk

---

- **Magnetisierung:** Festplatte, Tonband
- **Reflexion von Licht:** CD, DVD (optische Datenträger)
- **elektrische Ladung:** Memory Stick, SSD (Flash-Speicher)
- **elektrische Spannung:** USB-Kabel, Leiterbahn, Mikrochip (Kupferkabel)
- **elektromagnetische Wellen:** Bluetooth, Glasfaserkabel, Mobilfunk, WLAN
- **Quantenzustände:** Quantencomputern (Qubits)

:::

[^1]: Quelle: [Wikipedia: Joseph-Marie Jacquard](https://de.wikipedia.org/wiki/Joseph-Marie_Jacquard)
[^2]: Quelle: [Wikipedia: Binärcode](https://de.wikipedia.org/wiki/Bin%C3%A4rcode)

[1]: ?context=/content/fundamentals/index&page=/content/fundamentals/binary
