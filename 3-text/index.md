# 3 Textcodierung

## Inhalt

- [3.1 Brailleschrift](?page=1-braille/)
- [3.2 Pentacode](?page=2-pentacode/)
- [3.3 ASCII](?page=3-ascii/)
- [3.4 Unicode](?page=4-unicode/)
- [:mdi-microsoft-windows: Praktische Übung (Windows)](?page=5a-practical-windows/)
- [:mdi-apple: Praktische Übung (macOS)](?page=5b-practical-macos/)
- [:exercise: Arbeitsblatt](?page=6-exercises/)
