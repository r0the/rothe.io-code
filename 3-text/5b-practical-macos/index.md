# :mdi-apple: Praktische Übung (macOS)

In dieser praktischen Übung speicherst du auf deinem Computer eine Textdatei und überprüfst anschliessend, ob die in der Datei gespeicherten Bits tatsächlich der Unicode/UTF-8-Codierung des Texts entsprechen.

::: exercise

#### :exercise: Aufgabe 1 – Texteditor öffnen

Um in macOS Textdateien erstellen und bearbeiten zu können, installierst du am besten die App _CotEditor_ aus dem App Store:

- [:link: CotEditor im AppStore](https://apps.apple.com/us/app/coteditor/id1024640650)

:::

::: exercise

#### :exercise: Aufgabe 2 – Text eingeben

Starte den CotEditor und gib einen kurzen Text ein. Dieser sollte nicht nur ASCII-Zeichen enthalten, sondern auch ein Umlaut wie «ä» und Emojis. Emojis kannst du beispielsweise auf der folgenden Webseite suchen und kopieren:

- [:link: Emojipedia](https://emojipedia.org)

Das sieht dann etwa so aus:

![](./coteditor-input.png)
:::

::: exercise

#### :exercise: Aufgabe 3 - Textdatei speichern

Speichere die Textdatei unter dem Namen **Unicode-Test.txt** indem du

- die Tastenkombination [Command]+[S] drückst oder
- den Menüpunkt **Datei ‣ Sichern …** auswählst.

![](./coteditor-save.png)
:::

::: exercise

#### :exercise: Aufgabe 4 - Datei als Bytes betrachten

Mit dem Online-Tool _HexEd.it_ kannst du eine beliebige Datei Byte für Byte betrachten:

- [:link: HexEd.it](https://hexed.it)

Klicke auf **Datei öffnen** und wähle die vorhin gespeicherte Textdatei aus. Nun siehst du die einzelnen Bytes, welche die Datei enthält, beispielsweise:

```
54 73 63 68 C3 BC 73 73 20 F0 9F 98 80 21
```

Die Bytes werden wie in der Informatik üblich als Hexadezimalzahlen angegeben. Natürlich kann man die Bytes auch binär darstellen:

```
01010100 01110011 01100011 01101000 11000011 10111100
01110011 01110011 00100000 11110000 10011111 10011000
10000000 00100001
```

In der binären Darstellung siehst du sofort die Zeichen, welche mehrere Bytes benötigen. Sie beginnen mit einer 1. So ist `11000011 10111100` die Codierung der Buchstabens «ü».

Nun kannst du mit folgenden Hilfsmitteln überprüfen, ob die Bytes tatsächlich der Unicode/UTF-8-Codierung deines Textes entsprechen:

- [:link: ASCII-Tabelle](?page=../3-ascii/)
- [:link: Unicode-Tabelle](https://unicode-table.com/de/)

---

Im vorliegenden Beispiel sieht das so aus:

| Byte(s)       | Zeichen |
| :------------ | :------ |
| `54`          | T       |
| `73`          | s       |
| `63`          | c       |
| `68`          | h       |
| `C3 BC`       | ü       |
| `73`          | s       |
| `73`          | s       |
| `20`          | ␣       |
| `F0 9F 98 80` | 😀      |
| `21`          | !       |

:::

::: exercise

#### :extra: Zusatzaufgabe - Andere Dateien

Betrachte andere Dateien in _HexEd.it_ und achte auf folgendes:

- Findest du in anderen Dateien auch Unicode-codierter Text?
- Erkennst du ein Muster bei den ersten paar Bytes von Dateien des gleichen Typs?

:::
