# :exercise: Arbeitsblatt «Sehsterne»

## Links

- [:pdf: Arbeitsblatt als PDF](./arbeitsblatt-textcodierung.pdf)

## Lösungen

::: exercise 1. ACINT-Code

---

1. **IN CINCINNATI**
2. ☆☆★&nbsp;&nbsp;☆☆☆&nbsp;&nbsp; ☆★☆&nbsp;&nbsp;☆☆★&nbsp;&nbsp;★☆★&nbsp;&nbsp;☆☆☆&nbsp;&nbsp; ☆★☆&nbsp;&nbsp;☆☆★&nbsp;&nbsp;★☆☆&nbsp;&nbsp;☆☆☆&nbsp;&nbsp; ☆☆★&nbsp;&nbsp;☆★☆&nbsp;&nbsp;★☆★

:::

::: exercise 2. Gebärdensprache

---

1. Es gibt viele Ansätze. Eine einfache Möglichkeit ist, die zusätzlichen Buchstaben durch Heben zweier benachbarter Arme zu signalisieren:

   ![](material/images/see-star-letters-2.svg)

2. Wenn die Sehsterne einen binären Code wählen, können sie maximal $2^5 = 32$ verschiedene Zeichen darstellen.

:::

::: exercise 3. Erweiterter ACINT-Code

---

Auch hier gibt es verschiedene Ansätze. Drei Bit reichen nicht, da damit maximal $2^3 = 8$ Zeichen dargestellt werden können.

Eine Möglichkeit ist es, wie bei ASCII-Erweiterungen ein zusätzliches Bit voranzustellen. Bei den bisherigen Zeichen muss das Bit Null sein:

| Zeichen | Code | Zeichen | Code |
| ------: | :--- | ------: | :--- |
|  (leer) | ☆☆☆☆ |         |      |
|       A | ☆☆☆★ |       E | ★☆☆★ |
|       C | ☆☆★☆ |       H | ★☆★☆ |
|       I | ☆☆★★ |       J | ★☆★★ |
|       N | ☆★☆☆ |       M | ★★☆☆ |
|       T | ☆★☆★ |       R | ★★☆★ |

Eine andere Möglichkeit wäre, wie bei der Brailleschrift ein «Umschaltzeichen» einzuführen. Die Buchstaben E, H, J, M, R erhalten den gleichen Code wie A,C, I, N, T. Mit einem vorangestellten ★★★ wird markiert, dass der «neue» Buchstabe gemein ist:

| Zeichen | Code | Zeichen | Code    |
| ------: | :--- | ------: | :------ |
|  (leer) | ☆☆☆  |         |         |
|       A | ☆☆★  |       E | ★★★ ☆☆★ |
|       C | ☆★☆  |       H | ★★★ ☆★☆ |
|       I | ☆★★  |       J | ★★★ ☆★★ |
|       N | ★☆☆  |       M | ★★★ ★☆☆ |
|       T | ★☆★  |       R | ★★★ ★☆★ |

:::

::: exercise 4. Gesunkenes Schiff (Bonus)

---

Aus den Buchstaben A, C, I, N, T lässt sich **TITANIC** zusammensetzen.
:::
