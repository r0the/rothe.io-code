# 3.1 Brailleschrift[^1]

Die Brailleschrift wird von Blinden und stark Sehbehinderten benutzt, ist also eine Blindenschrift. Sie wurde 1825 von dem Franzosen Louis Braille entwickelt. Die Schrift besteht aus Punktmustern, die, meist von hinten in das Papier gepresst, mit den Fingerspitzen als Erhöhungen zu ertasten sind.

Sechs Punkte, drei in der Höhe mal zwei Punkte in der Breite, bilden das Raster für die Punkte-Kombinationen, mit denen die Zeichen (Buchstaben, Ziffern, Leerzeichen, …) dargestellt werden. Bei sechs Punkten sind $2^6 = 64$ verschiedene Zeichen darstellbar.

Das Leerzeichen wird hier mit dem Zeichen ␣ dargestellt.

::: columns 6
![␣](./braille/space.svg)

---

![a oder 1](./braille/a.svg)

---

![b oder 2](./braille/b.svg)

---

![c oder 3](./braille/c.svg)

---

![d oder 4](./braille/d.svg)

---

![e oder 5](./braille/e.svg)

---

![f oder 6](./braille/f.svg)

---

![g oder 7](./braille/g.svg)

---

![h oder 8](./braille/h.svg)

---

![i oder 9](./braille/i.svg)

---

![j oder 0](./braille/j.svg)

---

![k](./braille/k.svg)

---

![l](./braille/l.svg)

---

![m](./braille/m.svg)

---

![n](./braille/n.svg)

---

![o](./braille/o.svg)

---

![p](./braille/p.svg)

---

![q](./braille/q.svg)

---

![r](./braille/r.svg)

---

![s](./braille/s.svg)

---

![t](./braille/t.svg)

---

![u](./braille/u.svg)

---

![v](./braille/v.svg)

---

![w](./braille/w.svg)

---

![x](./braille/x.svg)

---

![y](./braille/y.svg)

---

![z](./braille/z.svg)

---

![ä](./braille/a-umlaut.svg)

---

![ö](./braille/o-umlaut.svg)

---

![ü](./braille/u-umlaut.svg)

---

![.](./braille/point.svg)

---

![,](./braille/comma.svg)

---

![-](./braille/hyphen.svg)

---

![?](./braille/question-mark.svg)

---

![$](./braille/dollar.svg)

---

![#](./braille/hash.svg)
:::

## Darstellung von Zahlen

In der Brailleschrift werden für die Ziffern die gleichen Muster wie für die Buchstaben a bis j verwendet. Eine Zahl wird markiert, indem man ihr ein Hashtag-Zeichen `#` voranstellt.

So bedeutet dieser Braille-Text «bach»

![b](./braille/b.svg)
![a](./braille/a.svg)
![c](./braille/c.svg)
![h](./braille/h.svg)

und dieser «2138»:

![#](./braille/hash.svg)
![2](./braille/b.svg)
![1](./braille/a.svg)
![3](./braille/c.svg)
![8](./braille/h.svg)

## Darstellung von Grossbuchstaben

Auf eine ähnliche Weise werden Grossbuchstaben dargestellt. Ein Dollarzeichen `$` bedeutet, dass der nächste Buchstabe gross geschrieben wird.

Der Grossbuchstabe `B` wird in Braille also als `$b` geschrieben und der folgende Braille-Text bedeutet «Bach»:

![$](./braille/dollar.svg)
![b](./braille/b.svg)
![a](./braille/a.svg)
![c](./braille/c.svg)
![h](./braille/h.svg)

::: info Escape-Zeichen
In der Informatik wird ein Zeichen wie `$`, welches die Bedeutung von nachfolgenden Zeichen ändert, als **Escape**-Zeichen bezeichnet.

Escape-Zeichen werden häufig in Programmiersprachen eingesetzt. In den sozialen Medien wird mit den Escape-Zeichen `#` und `@` markiert, dass das nachfolgende Wort ein Suchbegriff bzw. ein Benutzername ist.
:::

## 6-Bit-Darstellung der Brailleschrift

Wir können die sechs Bit der Brailleschrift auch mit Nullen und Einsen darstellen. Die schwarzen Punkte (Erhöhungen) werden durch eine Eins, die weissen Punkte durch eine Null dargestellt.

Für das Zeichen «N» ergibt sich also das Bitmuster `110110`.

Die folgende Tabelle zeigt die 6-Bit-Darstellung der Brailleschrift:

| Zeichen | Code     | Zeichen | Code     | Zeichen | Code     |
| ------: | :------- | ------: | :------- | ------: | :------- |
|       ⎵ | `000000` |       l | `101010` |       x | `110011` |
|   a / 1 | `100000` |       m | `110010` |       y | `110111` |
|   b / 2 | `101000` |       n | `110110` |       z | `100111` |
|   c / 3 | `110000` |       o | `100110` |       ä | `010110` |
|   d / 4 | `110100` |       p | `111010` |       ö | `011001` |
|   e / 5 | `100100` |       q | `111110` |       ü | `101101` |
|   f / 6 | `111000` |       r | `101110` |       . | `010000` |
|   g / 7 | `111100` |       s | `011010` |       , | `001000` |
|   h / 8 | `101100` |       t | `011110` |       - | `000011` |
|   i / 9 | `011000` |       u | `100011` |       ? | `001001` |
|   j / 0 | `011100` |       v | `101011` |       $ | `010001` |
|       k | `100010` |       w | `011101` |       # | `010111` |

::: box warning

#### :important: Wichtige Erkenntnisse

- Zeichen werden in eine fixe Anzahl Bits übersetzt.
- Mit $n$ Bits können maximal $2^n$ verschiedene Zeichen dargestellt werden.
- Es gibt spezielle Zeichen (Präfixe), welche die Bedeutung der nachfolgenden Bits ändern.
  :::

[^1]: Quelle: [Wikipedia: Brailleschrift](https://de.wikipedia.org/wiki/Brailleschrift)
