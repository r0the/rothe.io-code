# 3.2 Pentacode

Pentacode ist eine einfache Textcodierung, welche als Anschauungsbeispiel für den Informatik-Unterricht entworfen wurde.

In Pentacode werden Zeichen durch fünf Bit dargestellt. Mit fünf Bit können insgesamt $2^5 = 32$ Zeichen dargestellt werden. Die folgende Tabelle gibt alle Übersetzungen an.

Das Leerzeichen wird wie üblich mit ␣ dargestellt.

| Zeichen | Code    | Zeichen | Code    | Zeichen | Code    | Zeichen | Code    |
| ------: | :------ | ------: | :------ | ------: | :------ | ------: | :------ |
|       ␣ | `00000` |       H | `01000` |       P | `10000` |       X | `11000` |
|       A | `00001` |       I | `01001` |       Q | `10001` |       Y | `11001` |
|       B | `00010` |       J | `01010` |       R | `10010` |       Z | `11010` |
|       C | `00011` |       K | `01011` |       S | `10011` |       , | `11011` |
|       D | `00100` |       L | `01100` |       T | `10100` |       - | `11100` |
|       E | `00101` |       M | `01101` |       U | `10101` |       . | `11101` |
|       F | `00110` |       N | `01110` |       V | `10110` |       ? | `11110` |
|       G | `00111` |       O | `01111` |       W | `10111` |       @ | `11111` |

## Binärbaum

Ein Binärbaum ist eine Datenstruktur, d.h. eine Darstellungsform für Daten.

Ein Binärbaum eignet sich, um eine binäre Codierung darzustellen und zu decodieren. Der binäre Baum wird parallel zu den binär codierten Daten von oben nach unten durchgegangen. Wenn eine Null auftritt, geht man im Baum nach links, wenn eine Eins auftritt, nach rechts.

Wenn man bei einem Zeichen ankommt, weiss man, dass die abgearbeiteten Bits dieses Zeichen darstellen.

Unten ist der Binärbaum für den Pentacode abgebildet. Aus Platzgründen sind zwei Äste des Baumes unterhalb des Baumes platziert worden. Mit dem kleinen grünen und roten Kreis wird markiert, wo die Äste eigentlich hingehören. D.h. vom grünen Kreis wird zum anderen grünen Kreis gesprungen, und analog mit rot.

![](./binary-tree-pentacode.svg)

## Interaktiv

Hier kann der Pentacode ausprobiert werden:

<v-pentacode/>
