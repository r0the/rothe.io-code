# 3.4 Unicode / UTF-8

## Unicode

[Unicode][1] ist ein internationaler Standard für Schriftzeichen und Symbole. Das Unicode-Konsortium erstellt einen Katalog von allen sinnvollen Schriftzeichen, welcher ständig erweitert wird. In der Version 14, welche im September 2021 veröffentlicht wurde, umfasst Unicode 144'697 Zeichen.

Hier sind ein paar Zeichen aufgeführt, um zu illustrieren, wie umfangreich Unicode ist:

::: cards 4
[![](./unicode-0041.png)][10]

[Lateinischer Grossbuchstabe A][10]

---

[![](./unicode-3084.png)][11]

[Hiragana-Buchstabe Ya][11]

---

[![](./unicode-13CD.png)][12]

[Cherokee-Buchstabe S][12]

---

[![](./unicode-1F0B9.png)][13]

[Spielkarte Neun der Herzen][13]

---

[![](./unicode-1F92F.png)][14]

[Entsetztes Gesicht mit explodierendem Kopf][14]

---

[![](./unicode-1F412.png)][15]

[Affe][15]

---

[![](./unicode-23FB.png)][16]

[Power-Symbol][16]

---

[![](./unicode-1322C.png)][17]

[Ägyptische Hieroplyphe Nl012][17]
:::

Quelle: [UT - Unicode Table][2]

Jedes Unicode-Zeichen hat eine eindeutige Unicode-Nummer, welche häufig als hexadezimale Zahl geschrieben wird. Hier wird die dezimale Schreibweise verwendet.

## UTF-8

| Zeichen                             | Nummer | Bitmuster                             |
| :---------------------------------- | -----: | :------------------------------------ |
| A                                   |     65 | `01000001`                            |
| ä                                   |    228 | `11000011 10100100`                   |
| Schwarze Sonne mit Strahlen &#9728; |   9728 | `11100010 10011000 10000000`          |
| Affe &#128018;                      | 128018 | `11110000 10011111 10010000 10010010` |

**UTF-8** ist ein Code, der Unicode-Zeichen in Bits übersetzt. Ein Unicode-Zeichen wird mit ein bis vier Byte dargestellt.

# Begriffe

---

Wir wollen die grundlegenden Begriffe noch anhand der Unicode/UTF-8-Codierung repetieren:
Die **Senderin** will die **Information** «Ich bin müde» an den **Empfänger** übermitteln. Sie **codiert** die Information als Emoji. Das Handy codiert das Emoji mit dem Unicode/UTF-8-Standard als Bitfolge.

Die Bitfolge wird über das Internet an das Handy des Empfängers übermittelt. Das Handy des Empfängers **decodiert** die Bitfolge wieder zurück in das entsprechende Emoji. Die Interpretation des Emojis muss vom Empfänger selbst vorgenommen werden.

![](./unicode.svg)

## :extra: UTF-8 im Detail

Die folgende Tabelle zeigt, wie die Codierung funktioniert:

| Unicode-Bereich | Bitmuster                             | Anzahl Bit |
| :-------------- | :------------------------------------ | ---------: |
| 0 bis 127       | `0xxxxxxx`                            |          7 |
| 128 bis 2047    | `110xxxxx 10xxxxxx`                   |         11 |
| 2048 bis 65535  | `1110xxxx 10xxxxxx 10xxxxxx`          |         16 |
| ab 65536        | `11110xxx 10xxxxxx 10xxxxxx 10xxxxxx` |         21 |

Unicode-Zeichen mit einer Nummer zwischen 0 und 127 werden mit einem Byte dargestellt, welches mit `0` beginnt. Somit ist UTF-8 in diesem Bereich identisch mit ASCII.

Für die anderen Zeichen wird mehr als ein Byte verwendet. Dabei beginnt jedes Byte mit einer oder mehreren `1`, gefolgt von einer `0`. Die Anzahl `1` im ersten Byte definieren, wie viele Bytes für das Zeichen verwendet werden. Die folgenden Bytes werden mit `10` markiert. Die `x` werden mit der Binärdarstellung der Unicode-Nummer aufgefüllt.

Der Vorteil dieser Codierung ist, dass am Beginn eines Bytes erkannt wird, ob es sich um den Anfang die Fortsetzung eines Zeichens handelt.

| Beginn   | Bedeutung                                     |
| :------- | :-------------------------------------------- |
| `0…`     | ASCII-Zeichen                                 |
| `10…`    | Fortsetzung eines Zeichens mit mehreren Bytes |
| `110…`   | Beginn eines Zeichens mit zwei Bytes          |
| `1110…`  | Beginn eines Zeichens mit drei Bytes          |
| `11110…` | Beginn eines Zeichens mit vier Bytes          |

Dies wird deutlich, wenn man dies als Binärbaum darstellt:

![](./utf-8-prefix.svg)

[1]: https://de.wikipedia.org/wiki/Unicode
[2]: https://unicode-table.com/de/
[10]: https://unicode-table.com/de/0041/
[11]: https://unicode-table.com/de/3084/
[12]: https://unicode-table.com/de/13CD/
[13]: https://unicode-table.com/de/1F0B9/
[14]: https://unicode-table.com/de/1F92F/
[15]: https://unicode-table.com/de/1F412/
[16]: https://unicode-table.com/de/23FB/
[17]: https://unicode-table.com/de/1322C/
