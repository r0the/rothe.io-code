# Quellenangaben

## Pixel 4a

Autor: Spurrious Correlation
Lizenz: CC-BY-SA-4.0
Quelle: https://commons.wikimedia.org/wiki/File:Pixel_4a_front_schematic.svg

## iPhone 12

Autor: Rafael Fernandez
Lizenz: CC-BY-SA-4.0
Quelle: https://commons.wikimedia.org/wiki/File:IPhone_12_Blue.svg
