# Fehlerkorrektur
---

## Material

* [:pdf: Arbeitsblatt Partnerarbeit XO-Trick](arbeitsblatt-xo-trick.pdf)
* [:pdf: XO-Karten](xo-karten.pdf)

## Links

* [:link: Lehrer*innenfortbildung Baden-Württemberg][1]

[1]: https://lehrerfortbildung-bw.de/u_matnatech/imp/gym/bp2016/fb1/1_i1_duc/
