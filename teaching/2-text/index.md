# 2. Doppellektion – Textcodierung

## Möglicher Ablauf

- [:pdf: Präsentation Textcodierung](./2-textcodierung.pdf)
- [:pptx: Präsentation Textcodierung](./2-textcodierung.pptx)

* Repetition Binärsystem mit [Binär-Challenge](?page=../binary-challenge/)
* Einstieg Textcodierung (Varianten siehe unten)
* Besprechung
* Erklärung ASCII - Unicode
* [:pdf: Arbeitsblatt Textcodierung](../../3-text/6-exercises/arbeitsblatt-textcodierung.pdf)

## Einstieg Textcodierung (Variante Gruppenarbeit)

Einstieg mit entdeckendem Lernen als Gruppenarbeit

- [:pdf: Arbeitsblätter Gruppenarbeit](./einstieg-1/codes.pdf)
- [:odt: Arbeitsblätter Gruppenarbeit](./einstieg-1/codes.odt)
- [:pdf: Arbeitsblätter Gruppenarbeit mit Lösungen](./einstieg-1/codes-loesungen.pdf)
- [:odt: Arbeitsblätter Gruppenarbeit mit Lösungen](./einstieg-1/codes-loesungen.odt)

Links

- [:link: Lehrer\*innenfortbildung Baden-Württemberg][1]
- [:link: Computer Science Field Guide][2]
- [:link: inf-schule.de][3]

[1]: https://lehrerfortbildung-bw.de/u_matnatech/informatik/gym/bp2016/fb1/1_daten_code/
[2]: https://www.csfieldguide.org.nz/en/chapters/data-representation/
[3]: http://inf-schule.de/information/darstellunginformation

## Einstieg Textcodierung (Variante Einzelarbeit)

Einstieg mit entdeckendem Lernen als Einzelarbeit:

- [:pdf: Arbeitsblatt Einzelarbeit](./einstieg-2/ab-textcodierung.pdf)
