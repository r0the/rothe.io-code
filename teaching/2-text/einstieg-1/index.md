# Textcodierung
---

## Material

* [:pdf: Arbeitsblatt Gruppenarbeit Textcodierung](./codes.pdf)
* [:odt: Arbeitsblatt Gruppenarbeit Textcodierung](./codes.odt)
* [:pdf: Lösungen Arbeitsblatt Gruppenarbeit Textcodierung](./codes-loesungen.pdf)
* [:odt: Lösungen Arbeitsblatt Gruppenarbeit Textcodierung](./codes-loesungen.odt)

## Links

* [:link: Lehrer*innenfortbildung Baden-Württemberg][1]
* [:link: Computer Science Field Guide][2]
* [:link: inf-schule.de][3]

[1]: https://lehrerfortbildung-bw.de/u_matnatech/informatik/gym/bp2016/fb1/1_daten_code/
[2]: https://www.csfieldguide.org.nz/en/chapters/data-representation/
[3]: http://inf-schule.de/information/darstellunginformation
