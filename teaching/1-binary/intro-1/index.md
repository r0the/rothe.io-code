# Einführung Binärsystem (Variante 1)
---

## Auftrag
- Lesen Sie Ihr Blatt durch
- Person mit Blatt 2 führt den Zaubertrick vor.
- Andere Person versucht, den Trick zu erraten.
- Person mit Blatt 1 führt das binäre Zählen mit den Händen (Fingern) vor.
- Stellen Sie verschiedene Zahlen auf einer Zauberkarte mit den Händen das. Was fällt Ihnen auf?

## Material

* [:pdf: Arbeitsblätter Partnerarbeit Dualsystem](./partnerarbeit-dualsystem.pdf)
* [:pdf: Karten für Zaubertrick mit Binärzahlen](./zahlentrick-karten.pdf)
* [:docx: Karten für Zaubertrick mit Binärzahlen](./zahlentrick-karten.docx)
