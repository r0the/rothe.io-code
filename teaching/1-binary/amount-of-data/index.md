# Gruppenaktivität Datenmengen
---

## Ablauf
- Die Klasse wird in zwei Gruppen geteilt. Jede Schülerin und jeder Schüler erhält eine Datenmenge-Karte.
- Die Schüler*innen erhalten den Auftrag, sich in einer Reihe nach aufsteigender Datenmenge aufzustellen.
- Die resultierende Ordnung wird durch Vergleich der beiden Gruppe und der untenstehenden Tabelle diskutiert.

## Material

* [:pdf: Karten Datenmengen](./datenmenge-karten.pdf)

## Auflösung

### Alphabetisch

| ID  | Beschreibung                         |    Grösse |
| --- | ------------------------------------ | ---------:|
| A   | ja/nein                              |     1 Bit |
| B   | Vier Minuten Musik (mp3)             |      8 MB |
| C   | FullHD-Film auf Netflix (94 min)[^3] |    4.5 GB |
| D   | Tweet von Barak Obama                |     144 B |
| E   | Festplatte CHF 109.-[^5]             |      8 TB |
| F   | Google Rechenzentren[^6]             |     15 EB |
| G   | Apple Newton 2000 (1997)[^7]         |      1 MB |
| H   | Instagram-Foto 640x360 Pixel[^8]     |     80 kB |
| I   | Foto mit iPhone 6 erstellt[^9]       |      2 MB |
| J   | ein Buchstabe (UTF-8)                | 1 bis 4 B |
| K   | iPhone 11[^11]                       |    256 GB |
| L   | Windows 10-Installation[^12]         |     11 GB |

### Nach Grösse

| ID  | Beschreibung                         |    Grösse |
| --- | ------------------------------------ | ---------:|
| A   | ja/nein                              |     1 Bit |
| J   | ein Buchstabe (UTF-8)                | 1 bis 4 B |
| D   | Tweet von Barak Obama                |     144 B |
| H   | Instagram-Foto 640x360 Pixel[^8]     |     80 kB |
| G   | Apple Newton 2000 (1997)[^7]         |      1 MB |
| I   | Foto mit iPhone 6 erstellt[^9]       |      2 MB |
| B   | Vier Minuten Musik (mp3)             |      8 MB |
| C   | FullHD-Film auf Netflix (94 min)[^3] |    4.5 GB |
| L   | Windows 10-Installation[^12]         |     11 GB |
| T   | iPhone 11[^11]                       |    256 GB |
| E   | Festplatte CHF 109.-[^5]             |      8 TB |
| F   | Google Rechenzentren[^6]             |     15 EB |


## Bildnachweise

Sämtliche Icons (:mdi-twitter: :mdi-music: :mdi-instagram: :mdi-google: :mdi-microsoft-windows:) stammen aus der Schriftart [Material Design Icons][1], welche unter der SIL Open Font License 1.1 verfügbar ist.

Sämtliche Bilder sind Creative Commons-lizenziert, Logos stammen von Wikimedia Commons.

::: columns 4
![Altes Apple-Logo](./apple-logo-rainbow.svg)
***
![Instagram-Foto](./instagram-cc.jpg)
***
![Apple Newton](./apple-newton.jpg)
***
![iPhone 11](./iphone-11.svg)
***
![Festplatte](./hdd.svg)
***
![iPhone-Foto](./photo.jpg)
***
![Netflix-Logo](./netflix-logo.svg)
:::

[1]: https://materialdesignicons.com/

[^3]: Quelle: [Chip](https://praxistipps.chip.de/datenverbrauch-von-netflix-eine-einschaetzung_45631)
[^5]: Quelle: [Digitec](https://www.digitec.ch/de/s1/product/-5596288)
[^6]: Schätzung, Quelle: [XKCD](https://what-if.xkcd.com/63/)
[^7]: Quelle: [Wikipedia](https://en.wikipedia.org/wiki/MessagePad)
[^8]: Quelle: [Instagram](https://www.instagram.com/p/BKGqON5g_ep/)
[^9]: Quelle: [Digitec](https://www.digitec.ch/de/s1/product/-7331525)
[^11]: Quelle: [Wikipedia](https://de.wikipedia.org/wiki/IPhone_11)
[^12]: Quelle: [SoftwareOK](http://www.softwareok.com/?seite=faq-Windows-10&faq=31)
