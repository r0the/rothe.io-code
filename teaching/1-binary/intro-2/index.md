# Einführung Binärsystem (Variante 2)
---

Einführung in die Binärzahlen mit Karten mit Punkten nach einer Idee von Eisenmann[^1]:

![Karten mit Punkten ©](./dots.png)

## Material
* [:pdf: Einstieg Binärsystem (Auftrag und Punktekarten)](./einstieg_binaersystem.pdf)
* [:odt: Einstieg Binärsystem (Auftrag und Punktekarten)](./einstieg_binaersystem.odt)


## Auftrag
Lege zu jedem Auftrag die Kärtchen passend und schreibe dein Ergebnis auch mit Nullen und Einsen auf. Im Beispiel: Zahl 5 oder 0 0 1 0 1

1. Warum sind die Kärtchen so sortiert, wie wir es gemacht haben?
2. Wie heißt die kleinste Zahl aus dem Zehnersystem (Dezimalsystem), die du mit den Kärtchen legen kannst? Wie heißt die größte?
3. Gibt es eine Zahl zwischen kleinster und größter Zahl, die du mit den fünf Kärtchen nicht darstellen kannst? Begründe deine Antwort.
4. Zähle mit Hilfe der Kärtchen und beschreibe, was passiert.
5. Was passiert, wenn du eine gelegte Zahl mit 2 multiplizierst?
6. Wie viele Punkte wären auf einem 6. Kärtchen? Wie viele auf einem 7. usw.?
7. Wie verändert sich die größte darstellbare Zahl bei 6 Kärtchen? Wie bei 7 usw.?
8. Was haben alle geraden Zahlen gemeinsam? Was alle ungeraden?
9. Findest du besondere Zahlen? Wenn ja, welche? Beschreibe, was du beobachtest.

## Lösungen

1. Wenn keine Punkte mehr zu sehen sind, weiß man nicht, für was eine Eins bzw. eine Null stehen. Wie auch im Zehnersystem werden die Stellen festgelegt. Einer sind dabei ganz rechts.
2. Kleinste Zahl ist die 0: 0 0 0 0 0; größte Zahl die 31: 1 1 1 1 1
3. Man kann alle Zahlen mit den Kärtchen darstellen. Die Kärtchen mit den wenigen Punkten füllen immer den Rest auf. Beispiel: 3. Kärtchen hat vier Punkte, die beiden Kärtchen rechts daneben können die Zahlen 0 bis 3 darstellen. Also sind alle Zahlen von 4 bis 7 darstellbar und das nächstgrößere Kärtchen hat acht Punkte.
4. Jedes Kärtchen hat einen festen Rhythmus. Das rechte Kärtchen wird jedes Mal umgedreht (0 – 1 – 0 – 1 - …), das zweite von rechts jedes zweite Mal (0 – 0 – 1 – 1 – 0 – 0 – 1 – 1 …), das dritte jedes vierte Mal, das vierte jedes achte Mal, …
5. Die Einsen und Nullen der Ausgangszahl wandern alle um eine Stelle nach links und am Ende wird eine Null angehängt.
6. Auf einem 6. Kärtchen wären 32 Punkte (auf dem 7. 64, dem 8. 128, dem 9. 256,...)
   Regel: Kärtchen n hat 2n-1 Punkte.
8. Die größte darstellbare Zahl ist jeweils um eins kleiner als die Punktanzahl auf dem nächstgrößeren Kärtchen. Hier 31 (nächstgrößeres Kärtchen: 32), dann 63 (nächstgrößeres Kärtchen: 64), usw.
   Regel: Anzahl der Kärtchen ist n, dann ist die größte darstellbare Zahl 2n – 1
9. Alle geraden Zahlen haben am Ende eine Null, alle ungeraden eine Eins.
Mögliche Antworten: Zahlen mit lauter Einsen (siehe Aufgabe 7), Zweierpotenzen (eine Eins und lauter Nullen), besondere Zahlen im Zehnersystem, wie Prim- oder Quadratzahlen (im Binärsystem nichts besonderes?), usw.


[^1]: Quelle: [Lehrerfortbildung BW](https://lehrerfortbildung-bw.de/u_matnatech/informatik/gym/bp2016/fb1/1_daten_code/2_kopier/3_binaer/)
