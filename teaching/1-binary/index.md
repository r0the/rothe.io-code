# 1. Doppellektion – Binärsystem und Datenmengen

- [:pdf: Präsentation Datenmengen / Binärsystem](./1-einstieg.pdf)
- [:pptx: Präsentation Datenmengen / Binärsystem](./1-einstieg.pptx)

- [:pdf: Arbeitsblatt Zahlensysteme](ab-zahlensysteme/ab-zahlensysteme.pdf)
- [:pdf: Arbeitsblatt Zahlensysteme mit Lösungen](ab-zahlensysteme/ab-zahlensysteme-loesungen.pdf)

## Möglicher Ablauf

- Einstieg Jacquard, physikalische Darstellung von Bits
- Definition Byte
- Wissen abholen: kB, MB, GB, TB, Tabelle erarbeiten
- [Gruppenaktivität Datenmengen](?page=amount-of-data/)
- historische Binärpräfixe
- Einführung Binärsystem [Variante 1: PHSZ](?page=intro-1/) / [Variante 2: Eisenmann](?page=intro-2/)
- Umrechnung Dezimalzahl nach Binärzahl
