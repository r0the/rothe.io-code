# :goal: Lernziele

::: goal Lernziele Grundlagen

- Du weisst, dass ein Code eine **umkehrbare** Abbildungsvorschrift zwischen zwei Darstellungen von Information ist.
- Du kannst die SI-Einheiten für Datenmengen verwenden.
- Du kannst typische Datenmengen einschätzen.
- Du weisst, dass es dezimale und binäre Einheiten für Datenmengen gibt.

:::

::: goal Lernziele Zahlensysteme

- Du kannst erklären, was eine binäre Darstellung ist.
- Du weisst, wie ein Stellenwertsystem funktioniert.
- Du kannst natürliche Zahlen zwischen dezimaler, binärer und hexadezimaler Darstellung umrechnen.

:::

::: goal Lernziele Textcodierung

- Du kannst mit einer vorgegebenen Textcodierung kurze Texte codieren und decodieren.
- Du kennst die Zeichencodierungen ASCII und Unicode / UTF-8.
- Du verstehst, warum verschiedene Codierungen der gleichen Daten zu unterschiedlichen Datenmengen führen.
- Du kannst einen Binärbaum zur Decodierung von binären Daten einsetzen.

:::

::: goal Lernziele Grafikformate

- Du kennst die Farbmodelle Schwarzweiss, Graustufen RGB (True Colour) und CMYK.
- Du kannst den Speicherbedarf von Rastergrafiken in Abhängigkeit des Farbmodells berechnen.
- Du weisst, dass der Speicherbedarf von Rastergrafiken durch Kompression verkleinert werden kann.
- Du kannst einfache Rastergrafiken codieren und decodieren.
- Du kennst die unterschiedlichen Eigenschaften und Einsatzgebiete von Raster- und Vektorgrafiken.

:::

::: goal Lernziele Kompression

- Du weisst, dass häufige Zeichen weniger Information enthalten und dass diese Tatsache die Grundlage der Informationstheorie und somit von Kompressionsalgorithmen bildet.
- Du verstehst, wie die Huffmann-Codierung funktioniert.
- Du kannst zu kurzen Zeichenfolgen eine Huffmann-Codierung erstellen und diese anwenden.

:::

## Grundlagen

Diese Unterrichtseinheit deckt die folgenden Grobziele und Inhalte aus dem kantonalen Lehrplan[^1] ab:

> ### Information und Daten
>
> #### Grobziele
>
> Die Schülerinnen und Schüler
>
> - verstehen den Unterschied zwischen Information und Daten
> - sind mit verschiedenen Repräsentationsformen von Information vertraut
>
> #### Inhalte
>
> - Binärsystem
> - Unterscheidung digitale und analoge Repräsentationsformen
> - Codierung von Text und Bildern (z.B. ASCII, Unicode, Rastergrafik, Vektorgrafik)
> - Redundanz (z.B. Kompression, Fehlerkorrektur)

[^1]: Quelle: _[Lehrplan 17 für den gymnasialen Bildungsgang - Informatik][1], Erziehungsdirektion des Kantons Bern, S. 145 - 146_

[1]: https://www.erz.be.ch/erz/de/index/mittelschule/mittelschule/gymnasium/lehrplan_maturitaetsausbildung.html
