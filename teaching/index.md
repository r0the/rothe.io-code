# Material für Lehrpersonen

---

- [1. Doppellektion – Zahlensysteme, Datenmengen](?page=1-binary/)
- [2. Doppellektion – Textcodierung](?page=2-text/)
- [3. Doppellektion – Grafikformate](?page=3-graphics/)
- [4. Doppellektion – Kompression](?page=5-compression/)

- [:pdf: Arbeitsblatt Repetition](./ab-repetition/ab-repetition.pdf)

## TODO

### Aktivitäten / Material

- [Karten Datenträger](?page=physical-media/)
- [Binär-Challenge](?page=binary-challenge/)
- [Fehlerkorrektur](?page=correction)

::: warning

## Ideen

- zusätzlich Lauflängencodierung bei Grafik
- zusätzlich Dateiformate
- BARBIER BARBAROSSA
- RHABARBER

## Ideen aus Fachschaftstag

- Binäre Uhr Abenteuer Informatik
- Huffman: Mobilé

:::
