# Binär-Challenge

<v-binary-challenge/>

::: details Hinweise

- 24 A5-Karten mit einer weissen und einer farbigen Seite
- 4 SuS mit je einer Karte bilden zusammen eine 4 Bit Zahl (3er Gruppe, einer hat 2 Karten - die höchsten beiden Bit)
- Einwärmrunde von 0 bis 15 alle Zahlen anzeigen lassen
- Challenge
  - LP sagt eine Zahl
  - alle Personen einer Gruppe stehen in einer Reihe und bilden die Zahl binär nach
  - die Gruppe, welche diese als letzte anzeigt, scheidet aus usw.
  - Siegergruppe kriegt einen kleinen Preis
- Für LP: 6 Binärzahlen so schnell zu überblicken im Zimmer ist schwer, Variante: Zuerst erste 3 Gruppen gegen einander, dann zweite 3 Gruppen, dann mit 4 Gruppen bis zum Ende.
  :::
