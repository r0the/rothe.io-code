# Physische Datenträger

Vorlage für Karten mit verschiedenen Datenträgern.

Verwendung ist der eigenen Fantasie überlassen.

## Material

- [:pdf: Karten Datenträger](./datentraeger-karten.pdf)

## Bildnachweise

::: columns 4
![Kassette](./cassette.svg)

---

![Compact Disc](./compact-disc.svg)

---

![Festplatte](./harddisk.svg)

---

![Mobilfunkantenne](./mobile-antenna.jpg)

---

![Glasfaserkabel](./optical-fiber.jpg)

---

![Prozessor](./processor.jpg)

---

![Lochkarte](./punched-card.png)

---

![Arbeitsspeicher](./ram.jpg)
:::
