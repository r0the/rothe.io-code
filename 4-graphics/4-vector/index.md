# 4.4 Vektorgrafik

Eine SVG-Datei ist eine Textdatei, welche «Befehle» enthält, wie Formen gezeichnet werden sollen.

## Grundstruktur

Eine SVG-Datei hat folgende Grundstruktur:

```html
<svg
  xmlns="http://www.w3.org/2000/svg"
  xmlns:xlink="http://www.w3.org/1999/xlink"
></svg>
```

::: exercise Vorbereitung

- Öffnen Sie in Windows den Texteditor Notepad.
- Kopieren Sie die Grundstruktur in den Texteditor.
- Speichern Sie die Datei unter dem Namen **Aufgabe.svg**.
  :::

## Rechtecke

Das folgende Beispiel stellt die französische Flagge dar:

```html ./fr.svg

```

![](./fr.svg)

- `width="300"` legt die Breite des Bildes auf 300 Pixel fest.
- `height="200"` legt die Höhe des Bildes auf 200 Pixel fest.
- `<rect ...>` zeichnet ein Rechteck an den angegebenen Koordinaten mit der angegebenen Breite, Höhe und Farbe.

::: exercise Deutsche Flagge

Erstellen Sie eine SVG-Datei, welche die Deutsche Flagge darstellt. Die Flagge ist 500 Pixel breit, jeder Streifen ist 100 Pixel hoch. Das Rot ist rein (d.h. 255 Rot, 0 Grün und 0 Blau). Das Gelb hat einen Rotanteil von 255 und einen Grünanteil von 204.

![](./de.svg)
:::

## Grundformen

Auf der folgenden Seite sind die Grundformen von SVG zusammengefasst:

- [SVG-Grundformen](https://wiki.selfhtml.org/wiki/SVG/Elemente/Grundformen)

::: exercise Grundformen

Erstellen Sie eine neue SVG-Datei und probieren Sie die Grundformen aus.
:::

::: exercise Eigene Zeichnung

Erstellen Sie mit SVG eine winterliche oder festliche Grafik.
:::
