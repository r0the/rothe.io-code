# Affinity Photo installieren

1. Laden Sie das Installationsprogramm für Ihr Betriebssystem herunter:

   - [:mdi-microsoft-windows: Affinity Photo für Windows](https://store.serif.com/de/update/windows/photo/1/)
   - [:mdi-apple: Affinity Photo für macOS](https://store.serif.com/de/update/macos/photo/1/)

2. Führen Sie das Installationsprogramm aus.

   ::: warning
   **Achtung:** Es dauert eine Weile, bis das Installationsprogramm ein Fenster anzeigt. Haben Sie Gedulg und starten Sie das Installationsprogramm nicht mehrfach!
   :::

3. Klicken Sie hier auf **Installieren**. Falls Sie keine Verknüpfung auf dem Desktop wünschen, können Sie vorher das Häkchen entfernen.

   ![](./affinity-photo-install-1.png)

4. Klicken Sie auf **Ja**:

   ![](./affinity-photo-install-2.png)

5. Wenn Sie das folgende sehen, ist Affinity Photo fertig installiert worden. Löschen Sie nun im Ordner **Downloads** das Installationsprogramm **affinity-photo-1.10.4.exe**, dies wird nicht mehr benötigt.

   ![](./affinity-photo-install-3.png)

6. Starten Sie **Affinity Photo**, indem Sie das Startmenü öffnen und nach «Affinity» suchen. Beim ersten Start erscheint dieses Fenster. Klicken Sie auf **Ich habe einen Produktschlüssel**.

   ![](./affinity-photo-start-1.png)

7. Geben Sie bei _E-Mail/Organisation_ die E-Mail-Adresse __support@gymkirchenfeld.ch__ ein. Geben Sie bei _Produktschlüssel_ den aktuellen Produktschlüssel ein, den Sie im folgenden Dokument auf der intern-Seite finden:

   - [:pdf: Produktschlüssel Affinity](https://intern.gymkirchenfeld.ch/document/show?id=201102)

   ![](./affinity-photo-start-2.png)

8. Klicken Sie auf **Schließen**:

   ![](./affinity-photo-start-3.png)

9. Setzen Sie das Häkchen bei _Ich akzeptiere die Bedingungen der Lizenzvereinbarung_ und klicken Sie auf **Akzeptieren**.

   ![](./affinity-photo-start-4.png)

10. Entfernen Sie das Häkchen bei _Dieses Panel beim Start anzeigen_ und klicken Sie auf **Schließen**:

    ![](./affinity-photo-start-5.png)
