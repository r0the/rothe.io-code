# 4.1 Grundlagen

## Raster- und Vektorgrafik

Bei der Speicherung von Grafiken und Bildern im Computer wird zwischen zwei grundlegenden Darstellungsarten unterschieden: der Raster- und der Vektorgrafik.

::: columns 2
![Smiley als Rastergrafik](./smiley-raster.svg)

---

![Smiley als Vektorgrafik](./smiley-vektor.svg)
:::

## Rastergrafik

Bei einer Rastergrafik wird das Bild in viele kleine Quadrate unterteilt. Jedes Quadrat wird mit genau einer Farbe eingefärbt. Diese Quadrate werden **Pixel** genannt.

Die Anzahl Pixel, welche für eine Rastergrafik verwendet werden, wie als **Auflösung** des Bildes bezeichnet. Sie berechnet sich aus der Breite $b$ und Höhe $h$ der Grafik in Pixel.

Eine Rastergrafik mit hoher Auflösung hat eine bessere Qualität, benötigt allerdings auch mehr Speicherplatz. Den theoretische Speicherbedarf einer Rastergrafik hängt von der Anzahl Pixel und dem verwendeten Farbmodell ab.

Für jedes Pixel werden eine bestimmte Anzahl Bit zur Darstellung der Farben verwendet. Diese Anzahl wird **Farbtiefe** genannt. Typische Farbtiefen sind:

| Farbmodell            | Farbtiefe |           Platzbedarf in Byte |
| :-------------------- | --------: | ----------------------------: |
| Farbe                 |    3 Byte |           $b \cdot h \cdot 3$ |
| Farbe mit Transparenz |    4 Byte |           $b \cdot h \cdot 4$ |
| Graustufen            |    1 Byte |                   $b \cdot h$ |
| Schwarzweiss          |     1 Bit | $b \cdot h \cdot \frac{1}{8}$ |

Rastergrafiken werden in erster Linie zur Speicherung von Fotos verwendet.

## Vektorgrafik

Bei einer Vektorgrafik wird das darzustellende Bild aus geometrischen Objekten wie Linien, Kreise oder Rechtecken zusammengesetzt. Die einzelnen Objekte können einfarbig sein oder einen Farbverlauf haben, sowie teilweise transparent sein.

Vektorgrafiken haben zwei grosse Vorteile gegenüber Pixelgrafiken. Erstens benötigen sie normalerweise **deutlich weniger Speicherplatz** als eine entsprechende Pixelgrafik, da mit einem Objekt die Farbe von vielen Pixeln festgelegt wird.

Zweitens können Vektorgrafiken ohne Qualitätseinbusse **beliebig vergrössert und verkleinert** werden.

Vektorgrafiken werden deshalb insbesondere für folgende Anwendungen eingesetzt:

- Logos
- Illustrationen
- Symbole, Icons
- Schriftarten
- Strassen- und Landkarten
