# Affinity Photo verwenden

## Start

Wenn Sie Affinity Photo mit der Schullizenz verwenden, wird beim Start jeweils das folgende Fenster angezeigt. Klicken Sie hier einfach auf **Test fortsetzen**:

![](./affinity-photo-start.png)

## Grösse ändern

Wählen Sie den Menüeintrag **Dokument ‣ Dokumentgröße ändern…**, um die Grösse des Bildes anzupassen. Rechts neben _Größe_ kann die gewünschte Breite und Höhe eingegeben werden. Es können auch Prozentangabgen eingegeben werden, z.B. **50%**.

![Grösse ändern](./affinity-photo-groesse-aendern.png)

## Exportieren

Um ein Bild in einem bestimmten Format zu speichern, müssen Sie es exportieren mit dem Menüpunkt **Datei ‣ Exportieren…**. Im folgenden Fenster können Sie das gewünschte Dateiformat wählen. Für uns sind die Formate **PNG**, **JPEG** und **TGA** interessant.

![Bild exportieren](./affinity-photo-export-jpg.png)

Vor dem Export können Sie noch die gewünschte Auflösung (Breite und Höhe in Pixel) wählen. Unten im Fenster wird die geschätzte Dateigrösse für die eingestellte Auflösung angezeigt.

## Zuschneiden

Drücken Sie die Taste [C], um ein Bild zuzuschneiden. Anschliessen können Sie den gewünschten sichtbaren Bereich auswählen. Mit dem Knopf **Anwenden** oben links oder der Eingabestate [Enter] wird das Zuschneiden ausgeführt. Mit dem Knopf **Abbrechen** wird der Vorgang abgebrochen.

Um beim Zuschneiden ein bestimmtes Verhältnis einzuhalten, kann der Modus **Selbst definiertes Verhältnis** ausgewählt werden. Anschliessen kann rechts davon das gewünschte Verhältnis (z.B. 16×9) eingegeben werden.

![Zuschneiden eines Bildes](./affinity-photo-zuschneiden.png)
