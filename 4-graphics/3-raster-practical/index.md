# :exercise: Rastergrafik praktisch

::: exercise

#### :exercise: Aufgabe 1 – Installation Affinity Photo

Um Rastergrafiken zu bearbeiten wird die App [Affinity Photo](https://affinity.serif.com/de/photo/) empfohlen. Schüler:innen unserer Schule können diese App gratis verwenden.

Installieren Sie Affinity Photo mit [dieser Anleitung](?page=../affinity-photo-install/)
:::

::: exercise

#### :exercise: Aufgabe 2 – JPEG-Format

Das folgende Foto hat im Original eine Auflösung von 6016×4016 Pixel mit einer Farbtiefe von 3 Byte.

1. Berechnen Sie die theoretische Speichergrösse in Megabyte.
2. Laden Sie das Originalbild unter dem folgenden Link herunter:

   - [:link: Originalbild](assets/iceland.jpg)

3. Finden Sie heraus, wie gross der tatsächliche Speicherplatz des Bildes in Megabyte ist. Notieren Sie diese Zahl.
4. Öffnen Sie das Bild in Affinity Photo.
5. [Ändern Sie die Grösse](?page=../affinity-photo/) des Bildes auf 20%.
6. [Exportieren](?page=../affinity-photo/) Sie es mehrmals nacheinander als JPEG-Datei mit den Qualitäten 100%, 75%, 50% und 0%. Vergleichen Sie die Bilder und die Dateigrössen. Was stellen Sie fest?

![©](./iceland.jpg)
:::

## TGA-Grafikformat

Das _Targa Image File_ ist ein veraltetes Dateiformat, welches Pixeldaten umkomprimiert speichert. Deshalb können TGA-Dateien mit dem HexEd.It betrachtet und bearbeitet werden.

Der Dateikopf einer TGA-Datei enthält Informationen dazu, wie die Datei aufgebaut ist.

| Byte(s) | Bedeutung              | Beispiel |
| :------ | :--------------------- | :------- |
| 1       | Länge der Bild-ID      | 0        |
| 2       | Farbpalette vorhanden? | 0        |
| 3       | Bildtyp                | 2        |
| 4 - 8   | Farbpalette            | 0 0 0 0  |
| 9 - 12  | Koordinaten            | 0 0 0 0  |
| 13 - 14 | Breite                 | 10 00    |
| 15 - 16 | Höhe                   | 10 00    |
| 17      | Farbauflösung          | 18       |
| 18      | Alphakanal             | 20       |

Am wichtigsten sind die Felder «Breite» und «Höhe», welche die Dimension des Bildes angeben. Dabei sind die beiden Bytes vertauscht. Um die korrekte Zahl zu erhalten, müssen sie vertauscht und dann als Binärzahl interpretiert werden.

::: exercise

#### :exercise: TGA-Datei

1. Lade die folgende TGA-Datei herunter:

   - [:download: Vorlage](./vorlage.tga)

2. Öffne die Datei in _HexEd.it_:

   - [:link: HexEd.it](https://hexed.it)

3. Erstelle ein Bild, indem du die Farben für die einzelnen Pixel änderst.
   ![](./vorlage-tga-hexedit.svg)

4. Lade die geänderte Datei auf den Computer und öffne Sie mit Affinity Photo.

:::

::: exercise

#### :extra: Festliches Bild / Winterbild

Erstelle ein Bild, dass zu den Festtagen oder dem Winter passt. Schneide dazu Objekte aus verschiedenen Bildern aus und kombiniere sie

<v-video source="youtube" id="JmZ8534PlKI">
Objekte ausschneiden und in andere Bilder einfügen
</v-video>
:::
