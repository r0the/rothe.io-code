# 4 Grafikformate

- [4.1 Grundlagen](?page=1-basics/)
- [4.2 Farbdarstellung](?page=2-colour/)
- [4.3 Rastergrafik](?page=3-raster/)
- [:exercise: Rastergrafik praktisch](?page=3-raster-practical/)
- [4.4 Vektorgrafik](?page=4-vector/)
- [4.5 Dateiformate erkennen](?page=5-file/)

## Anleitungen

- [Affinity Photo installieren](?page=affinity-photo-install/)
- [Affinity Photo verwenden](?page=affinity-photo/)
